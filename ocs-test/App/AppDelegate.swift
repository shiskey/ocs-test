//
//  AppDelegate.swift
//  ocs-test
//
//  Created by mark on 12/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let jobListViewController = UIStoryboard(name: "JobStory", bundle: nil).instantiateInitialViewController()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = jobListViewController
        window?.makeKeyAndVisible()

        return true
    }
    
}
