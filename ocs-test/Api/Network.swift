//
//  Network.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Alamofire

struct Network {
    
    // GET request
    static func getRequest(urlString: String,
                           params: [String: String]?,
                           onSuccess: ((AnyHashable) -> ())?,
                           onFail: ((String) -> ())?) {
        
        let route: URL = URL(string: urlString)!
        var originalRequest = try! URLRequest(url: route, method: .get)
        originalRequest.cachePolicy = .reloadIgnoringCacheData      // Disable cache
        let encodedURLRequest = try! URLEncoding.default.encode(originalRequest, with: params)
        
        Alamofire.request(encodedURLRequest)
            .validate(statusCode: 200...304)
            .validate(contentType: ["application/json"])
            .responseJSON { r in
                switch r.result {
                case .success:
                    guard let json = r.result.value as? AnyHashable else {
                        print("""
                            >>> \(self) - \(#function): Made request for
                            \(String(describing: r.request?.url?.absoluteString))
                            and Cannot parse response
                            """)
                        onFail?("Cannot parse response")
                        return
                    }
                    
                    onSuccess?(json)
                case .failure(let error):
                    print("""
                        >>> \(self) - \(#function): Made request for
                        \(String(describing: r.request?.url?.absoluteString)) and got ERROR
                        \(error.localizedDescription)
                        """)
                    onFail?(error.localizedDescription)
                }
        }
    }
    
}
