//
//  Api.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import Alamofire

struct Api {
    
    private static let BASE_URL = "https://jobs.github.com/positions.json"
    
}

// MARK: job list api extension

extension Api {
    
    static func getJobsList(searchQuery: String, page: Int, onSuccess: (([Job]) -> ())?, onError: ((String) -> ())?) {
        
        let params = [
            "search": "\(searchQuery)",
            "page": "\(page)"
        ]
        
        Network.getRequest(urlString: BASE_URL, params: params, onSuccess: { response in
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .formatted(DateFormatter.githubDateFOrmat)
            
            if
                let jsonData = try? JSONSerialization.data(withJSONObject: response, options: []),
                let jobs = try? decoder.decode([Job].self, from: jsonData) {
                onSuccess?(jobs)
                return
            }
            
            onError?("Не удалось распарсить список вакансий")
        }) { errMessage in
            print(">>> \(self) - \(#function): Error - \(errMessage) ")
            onError?(errMessage)
        }
    }
    
}
