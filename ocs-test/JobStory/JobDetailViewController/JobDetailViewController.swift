//
//  JobDetailViewController.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit
import WebKit

class JobDetailViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var companyNameLabel: HeaderLabel!
    @IBOutlet var companyUrlButton: UIButton!
    @IBOutlet var titleLabel: TitleLabel!
    @IBOutlet var typeAndLocationLabel: SubTitleLabel!
    @IBOutlet var dateLabel: CaptionLabel!
    @IBOutlet var descriptionWebView: WKWebView!
    @IBOutlet weak var webViewConstraint: NSLayoutConstraint!
    
    var model: Job?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let model = self.model {
            if let logoUrl = model.companyLogo {
                self.logoImageView.load(from: logoUrl)
            } else {
                self.logoImageView.isHidden = true
            }
            self.companyNameLabel.text = model.company
            if let companyUrl = model.companyUrl {
                self.companyUrlButton.setTitle("(\(companyUrl.absoluteString))", for: .normal)
                self.companyUrlButton.addTarget(self, action: #selector(openCompanyWebSite), for: .touchUpInside)
            }
            self.titleLabel.text = model.title
            self.typeAndLocationLabel.text = "\(model.type) / \(model.location)"
            self.dateLabel.text = "\(model.createdAt.stringTimeIfToday())"
            self.descriptionWebView.navigationDelegate = self
            self.descriptionWebView.scrollView.isScrollEnabled = false
            self.descriptionWebView.scrollView.showsHorizontalScrollIndicator = false
            self.descriptionWebView.loadHTMLString(
                self.wrapHtml(description: model.description, howToApply: model.howToApply),
                baseURL: nil
            )
        }
    }
    
    @objc private func openCompanyWebSite() {
        if let companyUrl = self.model?.companyUrl {
            UIApplication.shared.open(companyUrl, options: [:], completionHandler: nil)
        }
    }
    
}

// MARK: WebView delegate

extension JobDetailViewController {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.webViewConstraint.constant = self.descriptionWebView.scrollView.contentSize.height
        }
    }
    
    private func wrapHtml(description: String, howToApply: String) -> String {
        let content =
        """
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no' />
        
                <script src="https://platform.twitter.com/widgets.js"></script>
                <script src="https://vk.com/js/api/openapi.js"></script>
                <script src="https://platform.instagram.com/en_US/embeds.js"></script>
                <script src=\"https://cdn.playbuzz.com/widget/feed.js\" async='async'></script>
        
                <style>
                    body {
                        line-height: 1.5;
                        font-family: -apple-system, system-ui, BlinkMacSystemFont;
                        font-size: 20px;
                    }
                    a {
                        color: #333333;
                    }
                </style>
            </head>
        
            <body>
                \(description)
                \(howToApply)
            </body>
        </html>
        """
        
        return content
    }
    
}
