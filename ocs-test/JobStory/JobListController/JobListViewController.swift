//
//  JobListViewController.swift
//  ocs-test
//
//  Created by mark on 12/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class JobListViewController: UITableViewController, UISearchBarDelegate {

    var model = JobListViewModel()
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup searchBar
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.delegate = self
        self.navigationItem.titleView = self.searchController.searchBar
        
        // setup tableView
        self.tableView.register(UINib(nibName: "JobCell", bundle: nil), forCellReuseIdentifier: JobCell.reuseId)
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.tableFooterView = UIView()
        
        self.model.onItemsReady = { [weak self] in
            self?.tableView.reloadData()
        }
        self.model.onError = { errMessage in
            print(">>> \(#file) - \(#function): Got Error: \(errMessage)")
        }
        self.model.loadJobList()
    }

}

// MARK: TableView delegate

extension JobListViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.getItemsCount()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: JobCell.reuseId, for: indexPath) as! JobCell
        cell.model = self.model.itemForIndex(index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.model.selectItemAt(index: indexPath.row)
        self.goToJobDetails()
    }
    
}

// MARK: ScrollView delegate

extension JobListViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let positionTrigger = scrollView.frame.height * 2
        
        if yOffset > contentHeight - positionTrigger {
            self.model.loadMore()
        }
    }
    
}


// MARK: Search controller delegate

extension JobListViewController {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let query = searchBar.text {
            UIView.performWithoutAnimation {
                self.tableView.scrollToRow(
                    at: IndexPath(row: NSNotFound, section: 0),
                    at: .top,
                    animated: false
                )
            }
            self.model.search(with: query)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.performWithoutAnimation {
            self.tableView.scrollToRow(
                at: IndexPath(row: NSNotFound, section: 0),
                at: .top,
                animated: false
            )
        }
        self.model.search(with: "")
    }

}

// MARK: Navigation

extension JobListViewController {
    
    private func goToJobDetails() {
        self.performSegue(withIdentifier: "JobDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let jobDetailViewController = segue.destination as? JobDetailViewController {
            jobDetailViewController.model = self.model.getSelectedItem()
        }
    }
    
}
