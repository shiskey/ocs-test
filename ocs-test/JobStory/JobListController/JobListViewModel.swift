//
//  JobListViewModel.swift
//  ocs-test
//
//  Created by mark on 12/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class JobListViewModel {
    
    var onItemsReady: (() -> ())?
    var onError: ((String) -> ())?
    
    private var items = [Job]()
    private var selectedItem: Job?
    private var canLoadMore = false
    private var currentQuery = ""
    private var currentPage = 1     // Github jobs api starts from "1"
    
}

// MARK: Api calls

extension JobListViewModel {
    
    private func load(with query: String, page: Int, onComplete: @escaping (([Job]) -> ())) {
        Api.getJobsList(searchQuery: query, page: page, onSuccess: { jobs in
            onComplete(jobs)
        }) { errMessage in
            self.onError?(errMessage)
        }
    }
    
}

// MARK: ViewController actions calls

extension JobListViewModel {
    
    func loadJobList() {
        self.load(with: self.currentQuery, page: self.currentPage, onComplete: { jobs in
            self.canLoadMore = jobs.count > 0
            self.items = jobs
            self.onItemsReady?()
        })
    }
    
    func search(with query: String) {
        self.load(with: query, page: self.currentPage, onComplete: { jobs in
            self.currentQuery = query
            self.currentPage = 1
            self.items = jobs
            self.onItemsReady?()
        })
    }
    
    func loadMore() {
        if self.canLoadMore {
            self.canLoadMore = false
            self.currentPage += 1
            self.load(with: self.currentQuery, page: self.currentPage, onComplete: { jobs in
                self.canLoadMore = jobs.count > 0
                self.items += jobs
                self.onItemsReady?()
            })
        }
    }
    
}

// MARK: Datasource

extension JobListViewModel {
    
    func getItemsCount() -> Int {
        return self.items.count
    }

    func itemForIndex(index: Int) -> Job {
        return self.items[index]
    }
    
    func selectItemAt(index: Int) {
        self.selectedItem = self.items[index]
    }
    
    func getSelectedItem() -> Job? {
        return self.selectedItem
    }
    
}
