//
//  Date.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

private let df = DateFormatter()

extension Date {
    
    func stringTimeIfToday() -> String {
        let isToday = Calendar.current.isDateInToday(self)
        isToday ? (df.dateFormat = "сегодня, HH:mm") : (df.dateFormat = "d MMMM, HH:mm")
        return df.string(from: self)
    }
    
}
