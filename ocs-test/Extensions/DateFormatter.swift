//
//  DateFormatter.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static let githubDateFOrmat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E MMM d HH:mm:ss zzz yyyy"
        return formatter
    }()
    
}
