//
//  UIImageView.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

private struct AssociatedKey {
    static var dataTask = "dataTask"
}


private struct ImageCache {
    
    private static let cache = NSCache<NSString, UIImage>()
    
    static func getImage(for key: String) -> UIImage? {
        if let img = ImageCache.cache.object(forKey: key as NSString) {
            return img
        }
        return nil
    }
    
    static func setImage(image: UIImage, for key: String) {
        ImageCache.cache.setObject(image, forKey: key as NSString)
    }
}


extension UIImageView {
    
    var dataTask: URLSessionDataTask? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.dataTask) as? URLSessionDataTask
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.dataTask, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func load(
        from url: URL,
        placeholder: UIImage? = nil,
        completionHandler: ((UIImage?) -> Void)? = nil) {
        image = placeholder
        let cacheKey = url.absoluteString
        
        // try to get from cache
        if let cachedImage = ImageCache.getImage(for: cacheKey) {
            self.image = cachedImage
            completionHandler?(self.image)
            return
        }
        
        // load
        self.dataTask = URLSession.shared.dataTask(with: url) { (data, response, _) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data,
                let image = UIImage(data: data)
                else {
                    completionHandler?(nil)
                    return
            }
            ImageCache.setImage(image: image, for: cacheKey)
            DispatchQueue.main.async {
                self.image = image
                completionHandler?(self.image)
            }
        }
        self.dataTask!.resume()
    }
    
    func cancelLoading() {
        if let dataTask = self.dataTask {
            dataTask.cancel()
        }
    }
    
}
