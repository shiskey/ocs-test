//
//  SubTitleLabel.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class SubTitleLabel: CustomLabel {
    
    class func getFont() -> UIFont {
        return UIFont.systemFont(ofSize: 12, weight: .medium)
    }
    
    override func setup() {
        super.setup()
        self.font = SubTitleLabel.getFont()
    }
    
}
