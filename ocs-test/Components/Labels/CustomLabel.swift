//
//  CustomLabel.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

@IBDesignable class CustomLabel: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        self.setup()
    }
    
    func setup() {
    }
    
    func setFontSize(fontSize: CGFloat) {
        self.font = UIFont(name: self.font.fontName, size: fontSize)
    }
    
    func setText(text: String, lineSpacing: CGFloat) {
        let attributedString = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        attributedString.addAttributes(
            [NSAttributedString.Key.paragraphStyle: paragraphStyle],
            range: NSMakeRange(0, text.count)
        )
        self.attributedText = attributedString
    }
    
}

