//
//  TitleLabel.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class TitleLabel: CustomLabel {
    
    class func getFont() -> UIFont {
        return UIFont.systemFont(ofSize: 18, weight: .bold)
    }
    
    override func setup() {
        super.setup()
        self.font = TitleLabel.getFont()
    }
    
}
