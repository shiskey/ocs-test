//
//  HeaderLabel.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class HeaderLabel: CustomLabel {
    
    class func getFont() -> UIFont {
        return UIFont.systemFont(ofSize: 20, weight: .black)
    }
    
    override func setup() {
        super.setup()
        self.font = HeaderLabel.getFont()
    }
    
}
