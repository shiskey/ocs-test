//
//  CaptionLabel.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class CaptionLabel: CustomLabel {
    
    class func getFont() -> UIFont {
        return UIFont.systemFont(ofSize: 12, weight: .heavy)
    }
    
    override func setup() {
        super.setup()
        self.font = CaptionLabel.getFont()
    }
    
}
