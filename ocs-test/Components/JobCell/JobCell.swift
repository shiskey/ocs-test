//
//  JobCell.swift
//  ocs-test
//
//  Created by mark on 13/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class JobCell: UITableViewCell {
    static let reuseId = "JobCellReuseId"
    
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var titleLabel: TitleLabel!
    @IBOutlet var locationLabel: SubTitleLabel!

    var model: Job? {
        didSet {
            guard let model = model else { return }
            if let logoUrl = model.companyLogo {
                self.logoImageView.isHidden = false
                self.logoImageView.load(from: logoUrl)
            } else {
                self.logoImageView.isHidden = true
            }
            self.titleLabel.text = model.title
            self.locationLabel.text = model.location
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.logoImageView.image = nil
        self.logoImageView.cancelLoading()
    }
    
}
