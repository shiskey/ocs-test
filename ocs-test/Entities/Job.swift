//
//  Job.swift
//  ocs-test
//
//  Created by mark on 12/01/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class Job: Decodable {
    
    let company: String
    let companyUrl: URL?
    let title: String
    let type: String
    let location: String
    let createdAt: Date
    let companyLogo: URL?
    let description: String
    let howToApply: String
    
}
